import geni.portal as portal
import geni.rspec.igext as IG

tourDescription = """

## Profile for deploying a single node

"""
tourInstructions = ""


class GLOBALS(object):
    DEFAULT_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-base-image"


pc = portal.Context()
pc.defineParameter("node_type", "hardware type to use", portal.ParameterType.STRING, "")
pc.defineParameter("component_id", "component ID to use", portal.ParameterType.STRING, "nuc1")
pc.defineParameter("component_manager_id", "component manager ID to use", portal.ParameterType.STRING, "urn:publicid:IDN+cpg.powderwireless.net+authority+cm")
pc.defineParameter("disk_image", "disk image to use", portal.ParameterType.STRING, GLOBALS.DEFAULT_IMG)
pc.defineParameter("rf_radiated",
                   "include rf radiated desire",
                   portal.ParameterType.BOOLEAN,
                   False)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

node = request.RawPC("node")
if params.node_type:
    node.hardware_type = params.node_type
else:
    node.component_id = params.component_id
    node.component_manager_id = params.component_manager_id

if params.rf_radiated:
    node.Desire("rf-radiated", 1.0)

node.disk_image = params.disk_image

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
